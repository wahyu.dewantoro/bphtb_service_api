<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mbpn extends CI_Model
{

    public $table = 'SSPD_FINAL';
    // public $id = 'ID_INC';
    // public $order = 'ASC';

    function __construct()
    {
        parent::__construct();
    }


    function getDataBPHTB($nop, $ntpd)
    {
     
            $final="SELECT ID, NO_REGISTRASI, NO_PELAYANAN, 
            ID_BILLING, TANGGAL, STATUS_DOKUMEN, 
            TAHUN, NO_PPAT, NAMA_PPAT, 
            NIK_WP, NAMA_WP, ALAMAT_WP, 
            RT_WP, RW_WP, KODE_KEL, 
            NAMA_KEL, KODE_KEC, NAMA_KEC, 
            KODE_POS, KODE_KAB, NAMA_KAB, 
            NOP, ALAMAT_OP, RT_OP, 
            RW_OP, KODE_KEL_OP, NAMA_KEL_OP, 
            KODE_KEC_OP, NAMA_KEC_OP, KODE_KAB_OP, 
            NAMA_KAB_OP, JENIS_PEROLEHAN, NOMOR_SERTIFIKAT, 
            NILAI_TRANSAKSI, LUAS_TANAH, LUAS_BANGUNAN, 
            NPOP_TANAH, NPOP_BANGUNAN, NJOP_PBB, 
            NPOP_PENGAJUAN, NPOP_PENELITIAN, NPOPTKP_PENGAJUAN, 
            NPOPTKP_PENELITIAN, NPOPKP_PENGAJUAN, NPOPKP_PENELITIAN, 
            BPHTB_PENGAJUAN, BPHTB_PENELITIAN, BPHTB_KURANG 
                         FROM rpt_SSPD_FINAL
                               UNION
                               SELECT ID, NO_REGISTRASI, NO_PELAYANAN, 
            ID_BILLING, TANGGAL, STATUS_DOKUMEN, 
            TAHUN, NO_PPAT, NAMA_PPAT, 
            NIK_WP, NAMA_WP, ALAMAT_WP, 
            RT_WP, RW_WP, KODE_KEL, 
            NAMA_KEL, KODE_KEC, NAMA_KEC, 
            KODE_POS, KODE_KAB, NAMA_KAB, 
            NOP, ALAMAT_OP, RT_OP, 
            RW_OP, KODE_KEL_OP, NAMA_KEL_OP, 
            KODE_KEC_OP, NAMA_KEC_OP, KODE_KAB_OP, 
            NAMA_KAB_OP, JENIS_PEROLEHAN, NOMOR_SERTIFIKAT, 
            NILAI_TRANSAKSI, LUAS_TANAH, LUAS_BANGUNAN, 
            NPOP_TANAH, NPOP_BANGUNAN, NJOP_PBB, 
            NPOP_PENGAJUAN, NPOP_PENELITIAN, NPOPTKP_PENGAJUAN, 
            NPOPTKP_PENELITIAN, NPOPKP_PENGAJUAN, NPOPKP_PENELITIAN, 
            BPHTB_PENGAJUAN, BPHTB_PENELITIAN, BPHTB_KURANG 
                               FROM SSPD_FINAL_OFFLINE";

         return $this->db->query(" SELECT *
        FROM (SELECT NOP,
                     NIK_WP NIK,
                     INITCAP (NAMA_WP) NAMA,
                     ALAMAT_WP ALAMAT,
                     INITCAP (NAMA_KEL_OP) KELURAHAN_OP,
                     INITCAP (NAMA_KEC_OP) KECAMATAN_OP,
                     'Kab. Malang' KOTA_OP,
                     LUAS_TANAH LUASTANAH,
                     LUAS_BANGUNAN LUASBANGUNAN,
                     round(CASE
                        WHEN BPHTB_PENELITIAN > 0 THEN BPHTB_PENELITIAN
                        ELSE BPHTB_KURANG
                     END,0)
                        PEMBAYARAN,
                     CASE WHEN NTPD IS NOT NULL THEN 'Y' ELSE 'T' END STATUS,
                     TO_CHAR (TGL_BAYAR, 'dd/mm/yyyy') TANGGAL_PEMBAYARAN,
                     CASE
                        WHEN NTPD IS NOT NULL THEN NTPD
                        ELSE NTPD_OFFLINE (A.ID)
                     END
                        NTPD,
                     CASE WHEN NTPD IS NOT NULL THEN 'L' ELSE 'H' END JENISBAYAR
                FROM (".$final.") A
                     JOIN V_NTPD ON V_NTPD.ID_BILLING = A.ID_BILLING)  where NOP=? and NTPD=?", array($nop, $ntpd))->row_array();
                      


    }


    function getDataPBB($VNOP)
    {
        return $this->db->query("SELECT KD_PROPINSI||KD_DATI2||KD_KECAMATAN||KD_KELURAHAN||KD_BLOK||NO_URUT||KD_JNS_OP NOP, NULL NIK_WP,NM_WP_SPPT NAMA_WP,
        INITCAP(GET_ALAMAT_OP('ALAMAT', KD_PROPINSI||KD_DATI2||KD_KECAMATAN||KD_KELURAHAN||KD_BLOK||NO_URUT||KD_JNS_OP)) ||' RT '||GET_ALAMAT_OP('RT', KD_PROPINSI||KD_DATI2||KD_KECAMATAN||KD_KELURAHAN||KD_BLOK||NO_URUT||KD_JNS_OP) ||' RW '||GET_ALAMAT_OP('RW', KD_PROPINSI||KD_DATI2||KD_KECAMATAN||KD_KELURAHAN||KD_BLOK||NO_URUT||KD_JNS_OP) ALAMAT_OP,
        GET_KELURAHAN(KD_KELURAHAN,KD_KECAMATAN) KELURAHAN_OP,GET_KECAMATAN(KD_KECAMATAN) KECAMATAN_OP, 'KAB. Malang' AS KOTA_KAB_OP,LUAS_BUMI_SPPT LUAS_TANAH_OP,LUAS_BNG_SPPT LUAS_BANGUNAN_OP,NJOP_BUMI_SPPT NJOP_TANAH_OP,NJOP_BNG_SPPT NJOP_BANGUNAN_OP,CASE WHEN STATUS_PEMBAYARAN_SPPT  ='1' THEN 'LUNAS' ELSE 'BELUM LUNAS' END STATUS_TUNGGAKAN
        FROM SPPT A WHERE THN_PAJAK_SPPT=TO_CHAR(SYSDATE,'YYYY')
        AND KD_PROPINSI =SUBSTR('$VNOP',1,2) 
                                 AND KD_DATI2=SUBSTR('$VNOP',3,2)
                                 AND KD_KECAMATAN=SUBSTR('$VNOP',5,3)
                                 AND KD_KELURAHAN=SUBSTR('$VNOP',8,3)
                                AND KD_BLOK=SUBSTR('$VNOP',11,3) 
                                AND NO_URUT=SUBSTR('$VNOP',14,4) 
                                AND KD_JNS_OP=SUBSTR('$VNOP',18,1)")->row();
    }

    function postBpn($data)
    {
        return $this->db->insert('DATA_BPN', $data);
    }


    function getTagihan($billing)
    {
        $this->db->where('ID_BILLING', $billing);
        return $this->db->get('TAGIHAN_SPPD')->row();
    }
}
