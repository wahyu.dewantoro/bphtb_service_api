<?php

defined('BASEPATH') or exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Servicebpn extends REST_Controller
{

    function __construct()
    {
        // Construct the parent class
        parent::__construct();

        // Configure limits on our controller methods
        // Ensure you have created the 'limits' table and enabled 'limits' within application/config/rest.php
        /* $this->methods['users_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['users_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['users_delete']['limit'] = 50; // 50 requests per hour per user/key */
    }

    // getBPHTBService
    public function bphtbservice_post()
    {
        $this->load->model('Mbpn');
        $res = json_decode(file_get_contents("php://input"), true);
        if (!empty($res['NOP']) && !empty($res['NTPD'])) {
            $row = $this->Mbpn->getDataBPHTB($res['NOP'], $res['NTPD']);
            if ($row) {
                $message = [
                    /* 'result' => array(
                        'NOP' => $row->NOP,
                        'NIK' => $row->NIK,
                        'NAMA' => trim($row->NAMA),
                        "ALAMAT" => $row->ALAMAT,
                        "KELURAHAN_OP" => $row->KELURAHAN_OP,
                        "KECAMATAN_OP" => $row->KECAMATAN_OP,
                        "KOTA_OP" => $row->KOTA_OP,
                        "LUASTANAH" => (float) $row->LUASTANAH,
                        "LUASBANGUNAN" => (float) $row->LUASBANGUNAN,
                        "PEMBAYARAN" => (int) $row->PEMBAYARAN,
                        "STATUS" => $row->STATUS,
                        "TANGGAL_PEMBAYARAN" => $row->TANGAL_PEMBAYARAN,
                        "NTPD" => $row->NTPD,
                        "JENISBAYAR" => $row->JENISBAYAR,
                    ), */
                    'result' => $row,
                    'respon_code' => 'OK'
                ];
                // $message=$this->db->last_query();
                $this->set_response($message, REST_Controller::HTTP_OK); // CREATED (201) being the HTTP response code
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No data were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'No data were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    // getPBBService
    public function pbbservice_post()
    {
        $this->load->model('Mbpn');

        $res = json_decode(file_get_contents("php://input"), true);
        // print_r($res);
        // die();
        // $nop = $this->get('nop');
        $nop = $res['nop'];
        if ($nop) {
            $row = $this->Mbpn->getDataPBB($nop);
            if ($row) {
                /*   $message = [
                    $row
                ]; */
                // $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
                $message = array(
                    'result' => array(
                        "NOP" => $row->NOP,
                        "NIK_WP" => $row->NIK_WP,
                        "NAMA_WP" => $row->NAMA_WP,
                        "ALAMAT_OP" => $row->ALAMAT_OP,
                        "KELURAHAN_OP" => $row->KELURAHAN_OP,
                        "KECAMATAN_OP" => $row->KECAMATAN_OP,
                        "KOTA_OP" => $row->KOTA_KAB_OP,
                        "LUAS_TANAH_OP" => (float) $row->LUAS_TANAH_OP,
                        "LUAS_BANGUNAN_OP" => (float) $row->LUAS_BANGUNAN_OP,
                        "NJOP_TANAH_OP" => (float) $row->NJOP_TANAH_OP,
                        "NJOP_BANGUNAN_OP" => (float) $row->NJOP_BANGUNAN_OP,
                        "STATUS_TUNGGAKAN" => $row->STATUS_TUNGGAKAN,
                    ),
                    'respon_code' => 'OK'
                );
                // $this->set_response($message, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
                $this->response($message, 200);
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'No data were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'No data were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }

    // PostDataBPN
    function bpntopemda_post()
    {
        $this->load->model('Mbpn');

        $res = json_decode(file_get_contents("php://input"), true);

        if (
            $res['AKTAID'] <> '' &&
            $res['TGL_AKTA'] <> '' &&
            $res['NOP'] <> '' &&

            $res['NAMA_WP'] <> '' &&
            $res['ALAMAT'] <> '' &&
            $res['KELURAHAN_OP'] <> '' &&
            $res['KECAMATAN_OP'] <> '' &&
            $res['KOTA_OP'] <> '' &&
            $res['LUASBANGUNAN_OP'] <> '' &&
            $res['LUASTANAH_OP'] <> '' &&
            $res['NO_SERTIPIKAT'] <> '' &&
            $res['NO_AKTA'] <> '' &&
            $res['PPAT'] <> ''
        ) {
            // insert data
            $data = array(
                'AKTA_ID' => $res['AKTAID'],
                'TGL_AKTA' => $res['TGL_AKTA'],
                'NOP' => $res['NOP'],
                'NIK' => $res['NIK'],
                'NPWP' => $res['NPWP'],
                'NAMA_WP' => $res['NAMA_WP'],
                'ALAMAT_OP' => $res['ALAMAT'],
                'KELURAHAN_OP' => $res['KELURAHAN_OP'],
                'KECAMATAN_OP' => $res['KECAMATAN_OP'],
                'KOTA_OP' => $res['KOTA_OP'],
                'LUASBANGUNAN_OP' => $res['LUASBANGUNAN_OP'],
                'LUASTANAH_OP' => $res['LUASTANAH_OP'],
                'NO_SERTIPIKAT' => $res['NO_SERTIPIKAT'],
                'NO_AKTA' => $res['NO_AKTA'],
                'PPAT' => $res['PPAT']
            );
            $res = $this->Mbpn->postBpn($data);
            if ($res) {
                $message = [
                    'status' => 1
                ];
                // $this->set_response($message, REST_Controller::HTTP_CREATED);
                $this->response($message, 200);
            } else {
                $this->response([
                    'status' => 1,
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code    
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'No data were found'
            ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
        }
    }
}
